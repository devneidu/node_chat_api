require('dotenv').config()
const path = require('path');
const express = require('express')
const app = express()
const port = 4000

const rootDir = require('./utils/path')

const multer = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'images')
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, file.fieldname + '-' + uniqueSuffix + file.originalname)
    }
})
const upload = multer({ storage: storage })

const mongoose = require('mongoose');

mongoose.set('strictQuery', true);

const MongoDBUri = `mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PASSWORD}@cluster0.er4fs.mongodb.net/${process.env.MONGODB_DOCUMENT_NAME}?retryWrites=true`;

app.use(express.json())
app.use(upload.single('image'))
app.use('/uploads',express.static(path.join(rootDir, 'uploads')))

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', '*');

    next()
})

/**
 * Routers
*/
const AuthRoutes = require('./routes/auth')

app.use('/auth', AuthRoutes)

app.use('/', (req, res) => {
    res.send('Hello World!')
})

app.use((error, req, res, next) => {
    const status = error.statusCode || 500
    const message = error.message
    const output = error.output

    console.log(error)
    res.status(status).json({
        message: message,
        output: output
    })
})

const uri = MongoDBUri;

mongoose.connect(uri, { keepAlive: true, keepAliveInitialDelay: 300000 }).then(() => {
    const server = app.listen(process.env.PORT || port, () => {
        console.log(`Example app listening on port ${process.env.PORT || port}`)
    })
    // const io = require('./socket').init(server)
    // io.on('connection', socket => {
    //     console.log('Client connected');
    // })
    console.log(server);
})
.catch(err => {
    console.log(err);
})
